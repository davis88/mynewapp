// THIS IS ANGULAR JS
"use strict";

(function () {
  "use strict";
  app.controller('MyNewAppController', MyNewAppController)

  // INJECTING $http
  MyNewAppController.inject = ["$http"]

  function MyNewAppController($http) {
    var self = this;
    self.loaded = true
  }

}
)()